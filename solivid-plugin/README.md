# Solivid.org Wordpress Plugin

Wordpress backend plugin for https://www.solivid.org/construim-un-mapa/

## CPT *initiative* 

The plugin defines a CPT called *initiative* based on ACF plugin.

## Custom API endpoint

The API offers the possiblity to retrieve all custom posts *initiative* as geojson.

`$ curl https://www.solivid.org/wp-json/solivid/v1/initiative/`
