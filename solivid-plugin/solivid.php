<?php
/*
Plugin Name: Solivid
Description: Custom post type for mapping project Solivid
Author: Gerald Kogler
Author URI: http://go.yuri.at
Text Domain: solivid
*/

/**
 * load textdomain
 */
add_action( 'init', 'solivid_load_textdomain' );
function solivid_load_textdomain() {
	load_plugin_textdomain('solivid', false, dirname(plugin_basename(__FILE__)));
}

/**
 * define CPT INITIATIVE
 */
function solivid_register_initiative() {

	register_taxonomy( 'initiative_field', 'initiative', array(
		'label'		   => 'Field',
        'rewrite'      => array( 'slug' => 'initiative_field' ),
		'show_in_rest' => true,
        'capabilities' => array(
			'assign_terms' => 'edit_initiatives',
			'edit_terms' => 'publish_initiatives'
		)
    ) );

	register_taxonomy( 'initiative_territory', 'initiative', array(
		'label'		   => 'Territorial scope',
        'rewrite'      => array( 'slug' => 'initiative_territory' ),
		'show_in_rest' => true,
        'capabilities' => array(
			'assign_terms' => 'edit_initiatives',
			'edit_terms' => 'publish_initiatives'
		)
    ) );

	$args = array(
		"label" => __( "Initiative", "solivid" ),
		"labels" => array(
			"name" => __( "Initiatives", "solivid" ),
			"singular_name" => __( "Initiative", "solivid" ),
		),
		"description" => "Solivid initiative",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "initiative",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "initiative",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "initiative", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-location",
		"supports" => array( "title" ),
		"taxonomies" => array( "initiative_field", "initiative_territory" ),
	);

	register_post_type( "initiative", $args );
}
add_action( 'init', 'solivid_register_initiative' );

/*function solivid_insert_terms() {
	// field
	wp_insert_term('Psychological support', 'initiative_field');
	wp_insert_term('Legal/Labour advice', 'initiative_field');
	wp_insert_term('Culture', 'initiative_field');
	wp_insert_term('Education', 'initiative_field');
	wp_insert_term('Sports', 'initiative_field');
	wp_insert_term('Production of medical equipment', 'initiative_field');
	wp_insert_term('Collection and/or distribution of food', 'initiative_field');
	wp_insert_term('Support to elderly', 'initiative_field');
	wp_insert_term('Support to homeless people', 'initiative_field');
	wp_insert_term('Support to childhood and teenagers at risk', 'initiative_field');
	wp_insert_term('Support to vulnerable groups', 'initiative_field');
	wp_insert_term('Gender violende', 'initiative_field');
	wp_insert_term('Other', 'initiative_field');

	// territory
	wp_insert_term('Neighbourhood', 'initiative_territory');
	wp_insert_term('Town/City', 'initiative_territory');
	wp_insert_term('Region', 'initiative_territory');
	wp_insert_term('Country', 'initiative_territory');
	wp_insert_term('World', 'initiative_territory');
}
add_action( 'init', 'solivid_insert_terms' );*/

/**
 * add capabilities for CPT INITIATIVE
 */
function solivid_add_cap_upload() {
	// admin
	$role = get_role( 'administrator' );

    $role->add_cap( 'edit_initiatives' ); 
    $role->add_cap( 'edit_published_initiatives' ); 
    $role->add_cap( 'edit_private_initiatives' ); 
    $role->add_cap( 'edit_others_initiatives' ); 
    $role->add_cap( 'publish_initiatives' ); 
    $role->add_cap( 'read_private_initiatives' ); 
    $role->add_cap( 'delete_private_initiatives' ); 
    $role->add_cap( 'delete_initiatives' ); 
    $role->add_cap( 'delete_published_initiatives' ); 
    $role->add_cap( 'delete_others_initiatives' );

	// admin
	$role = get_role( 'editor' );

    $role->add_cap( 'edit_initiatives' ); 
    $role->add_cap( 'edit_published_initiatives' ); 
    $role->add_cap( 'edit_private_initiatives' ); 
    $role->add_cap( 'edit_others_initiatives' ); 
    $role->add_cap( 'publish_initiatives' ); 
    $role->add_cap( 'read_private_initiatives' ); 
    $role->add_cap( 'delete_private_initiatives' ); 
    $role->add_cap( 'delete_initiatives' ); 
    $role->add_cap( 'delete_published_initiatives' ); 
    $role->add_cap( 'delete_others_initiatives' );

	// admin
	$role = get_role( 'author' );

    $role->add_cap( 'edit_initiatives' ); 
    $role->add_cap( 'edit_published_initiatives' ); 
    $role->add_cap( 'edit_private_initiatives' ); 
    $role->add_cap( 'edit_others_initiatives' ); 
    $role->add_cap( 'publish_initiatives' ); 
    $role->add_cap( 'read_private_initiatives' ); 
    $role->add_cap( 'delete_private_initiatives' ); 
    $role->add_cap( 'delete_initiatives' ); 
    $role->add_cap( 'delete_published_initiatives' ); 
    $role->add_cap( 'delete_others_initiatives' );
}
add_action( 'init', 'solivid_add_cap_upload' );

/**
 * add ACF fields to CPT INITIATIVE
 */
if( function_exists('acf_add_local_field_group') ) {

	acf_add_local_field_group(array(
		'key' => 'group_5eaa8ae439c05',
		'title' => 'Initiative',
		'fields' => array(
			array (
				'key' => 'field_5eaaddfad347c',
				'label' => 'Excel ID',
				'name' => 'excel_id',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'step' => 1,
			),
			array(
				'key' => 'field_5eaaccfad347c',
				'label' => 'Language',
				'name' => 'language',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'ca' => 'Català',
					'es' => 'Castellano',
					'en' => 'English',
					'fr' => 'Français',
					'it' => 'Italiano',
					'pt' => 'português',
				),
				'default_value' => array(
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'value',
				'ajax' => 0,
				'placeholder' => '',
			),
			array(
				'key' => 'field_5eaad65141710',
				'label' => 'Field',
				'name' => 'field',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'initiative_field',
				'field_type' => 'select',
				'allow_null' => 0,
				'add_term' => 0,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'object',
				'multiple' => 0,
			),
			array(
				'key' => 'field_5eb138392ff83',
				'label' => 'Field Other',
				'name' => 'field-other',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5eaacbecceyyy',
				'label' => 'Description',
				'name' => 'description',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
			array(
				'key' => 'field_5eaaca9ecexxx',
				'label' => 'Ubicació aproximada',
				'name' => 'location_aproximate',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5eaaca39ce3ba',
				'label' => 'Location',
				'name' => 'location',
				'type' => 'google_map',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'center_lat' => '41.4',
				'center_lng' => '2.15',
				'zoom' => 13,
				'height' => '',
			),
			array(
				'key' => 'field_5eaad6a041711',
				'label' => 'Territorial scope',
				'name' => 'territorial_scope',
				'type' => 'taxonomy',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'initiative_territory',
				'field_type' => 'radio',
				'allow_null' => 0,
				'add_term' => 0,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'object',
				'multiple' => 0,
			),
			array(
				'key' => 'field_5eaaca9ece3bb',
				'label' => 'Covered audience',
				'name' => 'covered_audience',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5eaacb7dce3bc',
				'label' => 'Contact/Information',
				'name' => 'contact_information',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
			array(
				'key' => 'field_5eaacb9bce3bd',
				'label' => 'Organization prior to Covid19',
				'name' => 'organization_prior_to_covid19',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => 'yes',
					'no' => 'no',
				),
				'allow_null' => 1,
				'other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
				'save_other_choice' => 0,
			),
			array(
				'key' => 'field_5eaacbd3ce3be',
				'label' => 'Start activity',
				'name' => 'start_activity',
				'type' => 'date_picker',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'display_format' => 'd/m/Y',
				'return_format' => 'd/m/Y',
				'first_day' => 1,
			),
			array(
				'key' => 'field_5eabf78d3a459',
				'label' => 'Aquesta experiència implica contacte físic entre les persones?',
				'name' => 'physical',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => 'yes',
					'no' => 'no',
				),
				'allow_null' => 1,
				'other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
				'save_other_choice' => 0,
			),
			array(
				'key' => 'field_5eabf7d03a45a',
				'label' => 'Si la vostra experiència implica contacte físic entre les persones, disposeu d\'algun protocol de seguretat per evitar el risc de contagi?',
				'name' => 'physical_security',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => 'yes',
					'no' => 'no',
				),
				'allow_null' => 1,
				'other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
				'save_other_choice' => 0,
			),
			array(
				'key' => 'field_5eabf8193a45b',
				'label' => 'Si la vostra experiència implica contacte físic entre les persones, disposeu del material necessari?',
				'name' => 'physical_material',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5eabf7d03a45a',
							'operator' => '==',
							'value' => 'yes',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'yes' => 'yes',
					'no' => 'no',
				),
				'allow_null' => 1,
				'other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
				'return_format' => 'value',
				'save_other_choice' => 0,
			),
			array(
				'key' => 'field_5eabf8493a45c',
				'label' => 'En cas afirmatiu, com l\'esteu aconseguint?',
				'name' => 'physical_material_how',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_5eabf8193a45b',
							'operator' => '==',
							'value' => 'yes',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
			array(
				'key' => 'field_5eaacbecce3bf',
				'label' => 'Collaborating organizations',
				'name' => 'collaborating_organizations',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
			array(
				'key' => 'field_5eaacc10ce3c0',
				'label' => 'Collaborating public bodies',
				'name' => 'collaborating_public_bodies',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
			array(
				'key' => 'field_5eaacc1cce3c1',
				'label' => 'How to collaborate',
				'name' => 'how_to_collaborate',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
			array(
				'key' => 'field_5eaacc1cce3h1',
				'label' => 'Comentaris o informació complementària (informació confidencial)',
				'name' => 'comment_confidential',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
			array(
				'key' => 'field_5eaacc1cce3h2',
				'label' => 'Dades de contacte (informació confidencial)',
				'name' => 'contact_confidential',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
			array(
				'key' => 'field_5eaacc1cce3h3',
				'label' => 'Dades per fer públic a la nostra web',
				'name' => 'data_web',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'br',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'initiative',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
}

/**
 * set google maps api key
 */
function solivid_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyAk5XwRCBSoh65Mtc5lMZYxnxmjhKZ5NfY');
}
add_action('acf/init', 'solivid_acf_init');

/**
 * remove initiatives taxonomies metabox
 */
function solivid_remove_default_category_metabox() {
	remove_meta_box( 'tagsdiv-initiative_field', 'initiative', 'normal' );
	remove_meta_box( 'tagsdiv-initiative_territory', 'initiative', 'normal' );
}
add_action( 'admin_menu' , 'solivid_remove_default_category_metabox' );

/**
 * add acf fields to Wordpress API
 */
function solivid_acf_to_rest_api($response, $post, $request) {
    if (!function_exists('get_fields')) return $response;

    if (isset($post)) {
        $acf = get_fields($post->id);
        $response->data['acf'] = $acf;
    }
    return $response;
}
add_filter('rest_prepare_post', 'solivid_acf_to_rest_api', 10, 3);


/**
 * register new rest api endpoint
 */
add_action( 'rest_api_init', function () {
	register_rest_route( 'solivid/v1', '/initiative', array(
		'methods' => 'GET',
		'callback' => 'solivid_initiative_api_endpoint',
	));
});


/**
 * custom API endpoint to retrieve initiatives as geojson
 * /wp-json/solivid/v1/initiative
 */
function solivid_initiative_api_endpoint( $request_data ) {

	// setup query argument
	$args = array(
		'post_type' => 'initiative',
		'posts_per_page' => 5000,
	);

    //trigger_error(json_encode($args), E_USER_WARNING);

	// Run a custom query
    $meta_query = new WP_Query($args);

    //trigger_error(json_encode($meta_query->request), E_USER_WARNING);

	$posts = $meta_query->get_posts();

	return (getGeojson($posts));
}

/**
 * convert posts to geojson for API
 */
function getGeojson($posts) {
	$features = array();

	// add features to geojson array
	foreach ($posts as $key => $post) {

		$props = get_fields($post->ID);
		$props['ID'] = $post->ID;
		$props['name'] = get_the_title($post->ID);
		$props['link'] = get_permalink($post->ID);

		$location = $props['location'];

		$features[] = array(
			'type' => 'Feature',
			'geometry' => array(
				'type' => 'Point',
				'coordinates' => array(
					(float)$location['lng'],
					(float)$location['lat']
				),
			),
			'properties' => $props
		);
	}

	$geojson = array(
		'type' => 'FeatureCollection',
		'name' => 'initiative',
		'features' => $features
	);

	return $geojson;
}
?>