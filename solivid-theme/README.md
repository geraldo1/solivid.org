# Solivid.org Wordpress Theme

Wordpress theme for https://www.solivid.org/ using custom plugin [Solivid](https://gitlab.com/geraldo1/solivid.org/solivid-plugin/)

Child theme for [Divi](https://www.elegantthemes.com/gallery/divi/).

## Page templates

- `Data Map`: Outputs all initiatives as map using [Openlayers](https://openlayers.org/) and loading initiatives from custom WP API endpoint https://www.solivid.org/wp-json/solivid/v1/initiative/.
- `Data Table`: Outputs all initiatives as table using [datatables library](https://datatables.net/)
- `Data Import`: Imports data from Google Drive using CSV and stores them as CPT initiative in WP backend.
- `Data Export`: Exports all registers from CPT initiative as Open Data. Example: https://www.solivid.org/export-initiatives-as-csv/
- `Data Export Private`: Exports all registers from CPT initiative with all (also private) fields for internal analysis.