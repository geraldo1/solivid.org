var url = "https://www.solivid.org/";
//var url = "http://localhost/solivid/";

jQuery(document).ready(function($) {
	var fields = null,
		labels = null,
		cartoLayer = null,
		bingLayer = null,
		lang = findGetParameter("lang");

	if (!lang) lang = "ca";

	/*
	 * Load thematic fields category for initiatives
	 */
	$.getJSON( url + "wp-json/wp/v2/initiative_field?per_page=13&orderby=id&lang="+lang, function( data ) {
		$(".loader").hide();		

		fields = data;

		/*
		 * Load translation for popup labels
		 */
		$.getJSON( "../wp-content/themes/Divi-child/js/labels.json", function( data ) {
			labels = data;

			// initiative layers
			$("#layerswitcher .content").append("<p><strong>"+labels["select"][lang]+" "+labels["field"][lang]+":</strong></p>");
			var html = "<ul class='layer'>";
			fields.forEach(function(field) {
				var slug = cleanLang(field.slug, lang);
				html += "<li data-id='"+field.id+"' class='"+slug+"'><img class='legend' src='"+url+"wp-content/themes/Divi-child/icons/"+slug+".png'/>"+field.name+"</li>";
			});
			html += "</ul>";
			$("#layerswitcher .content").append(html);

			$("#layerswitcher .layer li").click(function() {
				// toggle selected initiative icons
				$(this).find(".legend").toggleClass("off");
				initiativesLayer.getSource().changed();
				overlayPopup.setPosition(undefined);
			});

			// base layers
			$("#layerswitcher .content").append("<br><p><strong>"+labels["select"][lang]+":</strong></p>");
			html = "<ul class='base'>";
			html += "<li><input type='radio' name='base' value='carto' checked='checked'>Map (OSM/carto)</label></li>";
			html += "<li><input type='radio' name='base' value='bing'>Satellite (HERE/Bing)</label></li>";
			html += "</ul>";
			$("#layerswitcher .content").append(html);

			$("#layerswitcher .base li").click(function(event) {
				// toggle selected radio
				if (event.target.localName === "li") {
					$('input[name="base"]').not(':checked').prop("checked", true);
				}

				// change base layer selection
				var selected = $('input[name="base"]:checked').val();
				if (selected === "carto") {
					cartoLayer.setVisible(true);
					bingLayer.setVisible(false);
				}
				else {
					bingLayer.setVisible(true);
					cartoLayer.setVisible(false);				
				}
			});

			// close legend after 1 sec
			window.setTimeout(function(){ 
				$("#layerswitcher").removeClass("open");
				$(".legend-toggle").removeClass("open");
			}, 2000);
		});
	});

	/*
	 * Marker popup
	 */
	var overlayPopup = new ol.Overlay({
		element: document.getElementById('popup'),
		autoPan: true,
		autoPanAnimation: {
			duration: 250
		}
	});

	/*
	 * Define toggle legend control
	 */
	var LegendControl = /*@__PURE__*/(function (Control) {
		function LegendControl(opt_options) {
			var options = opt_options || {};

			var button = document.createElement('button');
			var element = document.createElement('div');
			element.className = 'legend-toggle ol-unselectable ol-control open';
			element.appendChild(button);

			ol.control.Control.call(this, {
				element: element,
				target: options.target
			});

			button.addEventListener('click', this.handleLegendToggle.bind(this), false);
		}

		if ( Control ) LegendControl.__proto__ = ol.control.Control;
		LegendControl.prototype = Object.create( ol.control.Control && ol.control.Control.prototype );
		LegendControl.prototype.constructor = LegendControl;

		LegendControl.prototype.handleLegendToggle = function handleLegendToggle () {
			$("#layerswitcher").toggleClass("open");
			$(".ol-zoom").toggleClass("open");
			$(".legend-toggle").toggleClass("open");
		};

	  	return LegendControl;
	}(ol.control.Control));

	/*
	 * Map
	 */
	cartoLayer = new ol.layer.Tile({
		source: new ol.source.XYZ({
		  //url: 'https://{1-4}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png',
		  url: 'https://{1-4}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png',
		  attributions: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, © <a href="https://carto.com/about-carto/">CARTO</a>',
		})
	});
	bingLayer = new ol.layer.Tile({
		visible: false,
		preload: Infinity,
		source: new ol.source.BingMaps({
			key: 'AtAGX4OD4E4uFsTleChcgtU60yMSZnUpazR_dsWC63L-FxZbXqRZJcZMXKCpp-Gm',
			//imagerySet: 'Aerial',
			imagerySet: 'AerialWithLabelsOnDemand',
		})
	});
		
	var maxZoom = 15;
	const map = new ol.Map({
		target: 'map',
		layers: [
			cartoLayer, 
			bingLayer
		],
		overlays: [
			overlayPopup,
		],
		controls: ol.control.defaults().extend([
		    new LegendControl()
		]),
		view: new ol.View({
			//center: ol.proj.fromLonLat([2.1374, 41.3971]),
			center: ol.proj.fromLonLat([3, 45]),
			zoom: 2,
			maxZoom: maxZoom,
		})
	});

	/*
	 * Marker icon style
	 */
	var initiativesStyleFunction = function(feature) {
		if (feature.get("field")) {
			if ($("li."+cleanLang(feature.get("field").slug)+" .legend").hasClass("off")) {
				return null;
			}

			// shadow
			var radius = 12;
			var blur = radius/3;
			var canvas = document.createElement('canvas');
		    canvas.width = 2*radius;
		    canvas.height = 2*radius;
			var context = canvas.getContext('2d');
			context.beginPath();
			context.clearRect(0, 0, canvas.width, canvas.height);
			context.scale(1,0.5);
			context.arc(radius, -radius, radius-blur, 0, 2 * Math.PI, false);
		    context.fillStyle = '#999';
			context.shadowColor = '#999';
			context.shadowBlur = 0.7*blur;
			context.shadowOffsetX = 0;
			context.shadowOffsetY = 1.5*radius;
			context.closePath();
		    context.fill();
			//context.shadowColor = 'transparent';

			// marker icons as images
			return [
				new ol.style.Style({
			    	image: new ol.style.Icon(({
						anchor: [0.5, 0.5],
						anchorXUnits: 'fraction',
						anchorYUnits: 'fraction',
						imgSize: [canvas.width, canvas.height], 
						img: canvas,  
					}))
			    }),
				new ol.style.Style({
			    	image: new ol.style.Icon({
					    anchor: [0.5, 37],
					    anchorXUnits: 'fraction',
					    anchorYUnits: 'pixels',
					    src: url+'wp-content/themes/Divi-child/markers/'+cleanLang(feature.get("field").slug)+'.png'
					})
				}),
			];
		}
		else {
			return null;
		}
	}

	/*
	 * Cluster style
	 */
	var initiativesLayer;
	var maxFeatureCount;
	var calculateClusterInfo = function(resolution) {
		maxFeatureCount = 0;
		var features = initiativesLayer.getSource().getFeatures();
		var feature, radius;
		for (var i = features.length - 1; i >= 0; --i) {
			feature = features[i];
			var originalFeatures = feature.get('features');
			var extent = ol.extent.createEmpty();
			var j = (void 0), jj = (void 0);
			for (j = 0, jj = originalFeatures.length; j < jj; ++j) {
				ol.extent.extend(extent, originalFeatures[j].getGeometry().getExtent());
			}
			maxFeatureCount = Math.max(maxFeatureCount, jj);
			//radius = 0.75 * (ol.extent.getWidth(extent) + ol.extent.getHeight(extent)) /
			    resolution;
			feature.set('radius', 25);
		}
	};

	var currentResolution;
	function clusterStyleFunction(feature, resolution) {
		if (resolution != currentResolution) {
			calculateClusterInfo(resolution);
			currentResolution = resolution;
		}
		
		var size = 0;
		var features = feature.get('features');
		//remove not selected categories
		for (var i = features.length - 1; i >= 0; --i) {
			if (!$("li."+cleanLang(features[i].get("field").slug)+" .legend").hasClass("off")) {
				size++;
			}
		}
		
		var style;
		if (size > 1 && map.getView().getZoom() < maxZoom) {
			// cluster style - but not on last zoom
			feature.set('cluster', true);
			style = new ol.style.Style({
				image: new ol.style.Circle({
				    radius: feature.get('radius'),
				    fill: new ol.style.Fill({
				    	color: [255, 153, 0, Math.min(0.8, 0.4 + (size / maxFeatureCount))]
				    })
			  	}),
				text: new ol.style.Text({
				    text: size.toString(),
				    fill: new ol.style.Fill({
						color: '#fff'
					}),
				    stroke: new ol.style.Stroke({
						color: 'rgba(0, 0, 0, 0.6)',
						width: 3
					})
				})
			});
		} else {
			// icon style
			feature.set('cluster', false);
			var features = feature.get('features');
			for (var i = features.length - 1; i >= 0; --i) {
				if (!$("li."+cleanLang(features[i].get("field").slug)+" .legend").hasClass("off")) {
					style = initiativesStyleFunction(features[i]);
					//break;
				}
			}
		}
		return style;
	}

	/*
	 * Initiatives layer with clusters and markers
	 */
	initiativesLayer = new ol.layer.Vector({
	    title: "Initiatives",
	    source: new ol.source.Cluster({
			distance: 50,
			source: new ol.source.Vector({
				url: url+'wp-json/solivid/v1/initiative?lang='+lang,
				format: new ol.format.GeoJSON(),
		    }),
		}),
	    style: clusterStyleFunction,
	});

	map.addLayer(initiativesLayer);

	/*
	 * Display mousepointer on mouseover
	 */
	map.on('pointermove', function(evt) {
	map.getTargetElement().style.cursor =
		map.hasFeatureAtPixel(evt.pixel) ? 'pointer' : '';
	});

	/*
	 * Select obra on click
	 */
	map.on('click', function(evt) {
		var feature = map.forEachFeatureAtPixel(evt.pixel,function(feature) {
        		return feature;
    		}
    	);

    	if (feature) {

    		if (feature.get("cluster") && map.getView().getZoom() < maxZoom) {
		    	// marker or cluster -> zoom to area
				map.getView().animate({
					zoom: map.getView().getZoom()+2, 
					center: evt.coordinate
				});
			}
			else {
	    		feature = feature.get("features")[0];
	    		//console.log(feature);

				// initiative -> show popup
				var title = "<span class='"+feature.get("field").slug+"'>";
				title += feature.get("name");
				title += "</span>";
				//title += "<img class='icon'";
				//title += " title='"+feature.get("field").name+"'";
				//title += " src='"+url+"wp-content/themes/Divi-child/icons/"+feature.get("field").slug+".png'>";

	    		document.getElementById('popup-title').innerHTML = title;
	    		
	    		var output = "";
	    		output += "<p><strong>"+labels["field"][lang]+"</strong>: "+feature.get("field").name;
	    		if (feature.get("field").slug === "other") {
	    			output += " - "+feature.get("field-other");
	    		}
	    		output += "</p>";
	    		
	    		output += "<p><strong>"+labels["location_aproximate"][lang]+"</strong>: "+feature.get("location_aproximate")+"</p>";

	    		output += "<p><strong>"+labels["territorial_scope"][lang]+"</strong>: "+feature.get("territorial_scope").name+"</p>";
	    		
	    		output += "<p>"+feature.get("description")+"</p>";

	    		output += "<p><strong>"+labels["contact_information"][lang]+"</strong>:<br>";
	    		output += addLinks(feature.get("contact_information"))+"</p>";

	    		document.getElementById('popup-content').innerHTML = output;

				overlayPopup.setPosition(evt.coordinate);
			}
    	}
    	else {
    		overlayPopup.setPosition(undefined);
    	}
	});

	/*
	 * Hide popup on changing zoom
	 */
	map.getView().on('change:resolution', function(e) {
		overlayPopup.setPosition(undefined);
	});

	/*
	 * Close popup
	 */
	$('#popup-closer').click(function() {
		overlayPopup.setPosition(undefined);
		$(this).blur();
		return false;
	});
});

/*
 * Remove lang extension from category
 */
function cleanLang(slug, lang=null) {
	var control = slug.split("-");
	var last = control[control.length-1];
	if (last === "br") {
		// exception for "pt-br"
		last = "pt-br";
	}

	// actual language or every language
	if ((!lang && 
		(last === "ca" || last === "es" || last === "en" ||
		 last === "fr" || last === "it" || last === "pt-br")) ||
		 last === lang) {

		// translated taxonomies have "-lang" added to the taxonomy slug
		slug = slug.substr(0, slug.length-last.length-1);
	}
	return slug;
}

/*
 * Get URL get parameter
 */
function findGetParameter(parameterName) {
  var result = null,
      tmp = [];
  //console.log(location.search);

  location.search
  .substr(1)
      .split("&")
      .forEach(function (item) {
      tmp = item.split("=");
      if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
  });
  return result;
}

/*
 * Convert http text to link in string
 */
function addLinks(text) {
	var output = "";
	var lines = text.split("<br />");
	lines.forEach(function(line) {
		var words = line.trim().split(" ");
		words.forEach(function(word) {
			word = word.trim();
			if (word.startsWith("http")) {
				output += "<a target='_blank' href='"+word+"'>"+word+"</a> ";
			}
			else {
				output += word+" ";
			}
		});
		output += "<br />";
	});

	return output;
}