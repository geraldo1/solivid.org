var url = "https://www.solivid.org/";
//var url = "http://localhost/solivid/";

jQuery(document).ready(function($) {
	$('#datatable').html('<table cellpadding="0" cellspacing="0" border="0" class="display" id="podcast-table"></table>');
	var datatable = $('#podcast-table').DataTable({
		paging: false,
		info: false,
		//order: [[ 2, "desc" ]],
		responsible: true,
		search: true,
		stateSave: false,
		ajax: { 
			url : url+"wp-json/wp/v2/initiative?per_page=100", 
			dataSrc: "",
			type : "GET",
		},
		columns: [
			{ "data": "title.rendered", "title" : "Name", "width": "200px"/*, "render": function ( data, type, row ) { 
					return "<a href='"+row["link"]+"'>"+data+"</a>"; 
				}, */
			},
			{ "data": "acf.description", "title" : "Descripción", "width": "500px"},
			{ "data": "acf.start_activity", "title" : "Start Activity", },
			{ "data": "acf.field.name", "title" : "Field", "render": function ( data, type, row ) { 
					if (data) return data;
					else return "";
				}, 
			},
			{ "data": "acf.territorial_scope.name", "title" : "Territorial Scope", "render": function ( data, type, row ) { 
					if (data) return data;
					else return "";
				}, 
			},
			/*{ "data": "acf.contact_information", "title" : "Contact/Information", "render": function ( data, type, row ) { 
					if (data) return data;
					else return "";
				}, 
			},*/
		],
	}).on( 'init.dt', function () {

		// show initiatives count
		$("#podcast-table_wrapper").prepend("<div class='dataTables_length'>"+datatable.page.info()["recordsTotal"]+" Initiatives</div>");
	});
});