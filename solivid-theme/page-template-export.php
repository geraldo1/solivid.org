<?php
/**
	Template Name: Export
*/

//header('Content-Encoding: UTF-8');
//header('Content-type: text/csv; charset=UTF-8');
header('Content-Encoding: Windows-1252');
header('Content-type: text/csv; charset=Windows-1252');
header('Content-Disposition: attachment; filename="Initiatives_Solivid.org.csv"');
header('Pragma: no-cache');
header('Expires: 0');

$fp = fopen('php://output', 'w');

// all published initiatives
$args = array(
	'post_type' => 'initiative',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'order' => 'ASC',
	'orderby' => 'ID',
);

$my_query = new WP_Query($args);

if ( $my_query->have_posts() ) {

	$list = array(
		array(
			'ID',
			'Title',
			'Language',
			'Field',
			'Field other',
			'Description',
			'Location aproximate',
			'Longitude',
			'Latitude',
			'Territorial Scope',
			'Contact/Information',
			'Organization prior to Covid19',
			'Start Activity',
			'Physical contact among people?',
			'Safety protocol?',
			'Do you have the necessary equipment?',
			'How are you obtaining it?',
			'Collaboration with other Institutions?',
			'Collaboration with public services?',
			'How people can collaborate?',
		),
	);

	$i = 1;
	while ($my_query->have_posts()) {

		$my_query->the_post();

		$fields = get_terms( 'initiative_field', array(
		    'hide_empty' => false
		));
		$field = get_post_meta(get_the_ID(), 'field', true);
		if ($field) {
			foreach ($fields as $term) {
				if ($term->term_id == $field) {
					$field = $term->name;
					break;
				}
			}
		}

		$territorial_scope = get_terms( 'initiative_territory', array(
		    'hide_empty' => false
		));
		$scope = get_post_meta(get_the_ID(), 'territorial_scope', true);
		if ($scope) {
			foreach ($territorial_scope as $term) {
				if ($term->term_id == $scope) {
					$scope = $term->name;
					break;
				}
			}
		}

		$location = get_post_meta(get_the_ID(), 'location', true);

		$list[] = array(
			'id' => $i,
			//'id' => get_the_ID(),
			'title' => html_entity_decode(get_the_title()),
			'language' => get_post_meta(get_the_ID(), 'language', true),
			'field' => $field,
			'field-other' => get_post_meta(get_the_ID(), 'field-other', true),
			'description' => get_post_meta(get_the_ID(), 'description', true),
			'location_aproximate' => get_post_meta(get_the_ID(), 'location_aproximate', true),
			'longitude' => (float)$location['lng'],
			'latitude' => (float)$location['lat'],
			'territorial_scope' => $scope,
			'contact_information' => get_post_meta(get_the_ID(), 'contact_information', true),
			'organization_prior_to_covid19' => printBoolean(get_post_meta(get_the_ID(), 'organization_prior_to_covid19', true)),
			'start_activity' => date('d/m/Y', strtotime(get_post_meta(get_the_ID(), 'start_activity', true))),
			'physical' => printBoolean(get_post_meta(get_the_ID(), 'physical', true)),
			'physical_security' => printBoolean(get_post_meta(get_the_ID(), 'physical_security', true)),
			'physical_material' => printBoolean(get_post_meta(get_the_ID(), 'physical_material', true)),
			'physical_material_how' => get_post_meta(get_the_ID(), 'physical_material_how', true),
			'collaborating_organizations' => get_post_meta(get_the_ID(), 'collaborating_organizations', true),
			'collaborating_public_bodies' => get_post_meta(get_the_ID(), 'collaborating_public_bodies', true),
			'how_to_collaborate' => get_post_meta(get_the_ID(), 'how_to_collaborate', true),			
		);

		$i++;
	}

	// output as CSV
	fwrite($fields, "sep=\t".PHP_EOL);
	foreach ($list as $fields) {
	    fputcsv($fp, $fields);
	}
}

function printBoolean($zerone) {
	if ($zerone == '' || $zerone == null) {
		return 'undefined';
	}
	else if ($zerone == 1 || $zerone == '1') {
		return 'yes';
	}
	else {
		return 'no';
	}
}

fclose($fp);

?>