<?php
/**
	Template Name: Import
*/

// only for logged users
if (!is_user_logged_in())
	wp_redirect(home_url());

get_header();
?>

<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">

			<div id="content" class="yeste-content" role="main">
				<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

				<header class="page-header">
					<h1 class="page-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->

				<div class="entry-content">

					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; // end of the loop. ?>

<?php

$max = 2;
if (isset($_GET['process'])) {
	if (is_numeric($_GET['process'])) {
		$max = (int)$_GET['process']+1;
	} 
	else if ($_GET['process'] == 'all') {
		$max = 1000;
	}
}

//$spreadsheet_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vT7zhw-SFySA40pgveYGSimsC5tHIAMjZ3qmC784rhM7aYVdc5QMRO6Bu3pe0gukZvcIoE7hTZf2gN-/pub?output=csv";

$spreadsheet_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vTi8jybuYULy3Pm8PAFkDGj0i1PxhqXP-xot_1ghi1hKYKx-6C0a2Rjlbz6ZKhTAJCWFp08bM8xxjHZ/pub?gid=1297183987&single=true&output=csv";

if(!ini_set('default_socket_timeout', 15)) echo "<!-- unable to change socket timeout -->";

if (($handle = fopen($spreadsheet_url, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $spreadsheet_data[] = $data;
    }
    fclose($handle);
}
else
    die("Problem reading csv");

//print_r($spreadsheet_data);

foreach ($spreadsheet_data as $i => $initiative) {

	$id = $initiative[0];

	// limit to rows
	if ($i > 0 && $i < $max) {

		//echo $id . "|" . $initiative[6] . "|" . $initiative[8] ."<br>";

		// check if initiative with Excel ID already exists
		$meta_query = new WP_Query(array(
			'post_type' => 'initiative',
			'meta_query' => array(
				array(
					'key'   => 'excel_id',
					'value' => (int)$id,
					'type' => NUMERIC,
				),
			)
		));

		// remove final characters like space and dot
		$title = rtrim(trim($initiative[6])," .\t\n\r\0\x0B");

		if (!$meta_query->have_posts()) {

			// initiative with Excel ID does not exist yet
			//print_r($initiative);
			$post = get_page_by_title($title, OBJECT, "initiative");
			
			if (!$post) {

				//insert posts into wordpress
				$post = array(
					'post_author' => 1,
					'post_status' => 'draft',
					'post_title' => $title,
					'post_type' => 'initiative',
				);
				$post_id = wp_insert_post($post);
				echo "<p><strong>".$id.": NEW initiative '".$title."'</strong> (WP ID: <a href='/wp-admin/post.php?post=".$post_id."&action=edit&lang=ca' target='_blank'>".$post_id."</a>)</p>";

				//add ACF meta data

				// Excel ID
				update_field("excel_id", $id, $post_id);

				// language
				$lang = $initiative[5];
				if ($lang == 'català') $lang = 'ca';
				else if ($lang == 'español') $lang = 'es';
				else if (strpos($lang, 'English') !== false) $lang = 'en';
				else if ($lang == 'français') $lang = 'fr';
				else if ($lang == 'italiano') $lang = 'it';
				else if (strpos($lang, 'português') !== false) $lang = 'pt';
				update_field("language", $lang, $post_id);

				// field
				$field = $initiative[7];
				$field_id_other = 84; //85; // ID of field "other"
				$field_id = $field_id_other;
				$terms = get_terms( array(
				    'taxonomy' => 'initiative_field',
				    'hide_empty' => false
				) );
				foreach ( $terms as $term ) {
					//echo $term->name."|".$field."<br>";
		            if ($term->name == $field) {
						$field_id = $term->term_id;
		            	break;
		            }
		        }
				update_field("field", $field_id, $post_id);
				if ($field_id == $field_id_other) {
					update_field("field-other", $field, $post_id);
				}

				// description
				update_field("description", $initiative[8], $post_id);

				// location_aproximate
				update_field("location_aproximate", $initiative[9], $post_id);

				// territorial_scope
				$scope = $initiative[10];
				if ($scope == 'Barri') $scope = 79; //2;
				else if ($scope == 'Municipi') $scope = 80; //3;
				else if ($scope == 'Regió') $scope = 81; //5;
				else if ($scope == 'País') $scope = 82; //4;
				else if ($scope == 'Món') $scope = 83; //6;
				update_field("territorial_scope", $scope, $post_id);

		        // covered_audience
		        update_field("covered_audience", $initiative[11], $post_id);

		        // contact_information
		        update_field("contact_information", $initiative[12], $post_id);

		        // organization_prior_to_covid19
		        $bool = $initiative[13];
		        if ($bool == "No") $bool = "no";
		        else if ($bool == "Si") $bool = "yes";
		        update_field("organization_prior_to_covid19", $bool, $post_id);

		        // start_activity
		        update_field("start_activity", $initiative[14], $post_id);

		        // physical
		        $bool = $initiative[15];
		        if ($bool == "No") $bool = "no";
		        else if ($bool == "Si") $bool = "yes";
		        update_field("physical", $bool, $post_id);

		        // physical_security
		        $bool = $initiative[16];
		        if ($bool == "No") $bool = "no";
		        else if ($bool == "Si") $bool = "yes";
		        update_field("physical_security", $bool, $post_id);

		        // physical_material
		        $bool = $initiative[17];
		        if ($bool == "No") $bool = "no";
		        else if ($bool == "Si") $bool = "yes";
		        update_field("physical_material", $bool, $post_id);

		        // physical_material_how
		        update_field("physical_material_how", $initiative[18], $post_id);

		        // collaborating_organizations
		        update_field("collaborating_organizations", $initiative[19], $post_id);

		        // collaborating_public_bodies
		        update_field("collaborating_public_bodies", $initiative[20], $post_id);

		        // how_to_collaborate
		        update_field("how_to_collaborate", $initiative[21], $post_id);

		        // comment_confidential
		        update_field("comment_confidential", $initiative[22], $post_id);

		        // contact_confidential
		        update_field("contact_confidential", $initiative[23], $post_id);

		        // data_web
		        update_field("data_web", $initiative[24], $post_id);

		        // coordinates
		        update_field("location", array(
		        	'lat' => $initiative[25],
		        	'lng' => $initiative[26],
		        ), $post_id);

		    }
		    else {
		    	echo "<p>".$id.": NOT (TITLE CHECK) Initiative '".$title."' already exists (WP ID <a href='/wp-admin/post.php?post=".$post->ID."&action=edit&lang=ca' target='_blank'>".$post->ID."</a>)</p>";
		    }
		}
	    else {
	    	echo "<p>".$id.": NOT (EXCEL ID CHECK) Initiative '".$title."' already exists (WP ID <a href='/wp-admin/post.php?post=".$post->ID."&action=edit&lang=ca' target='_blank'>".$post->ID."</a>)</p>";
	    }
	}
}

?>

				</div>

			</div><!-- #content -->

			<?php //get_yesdebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

</div> <!-- #main-content -->

<?php
get_footer();