<?php
/*
Template Name: Data Map
*/

get_header();

wp_register_style('ol-css', "https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.15.1/css/ol.css", array(), '6.15.1');
wp_enqueue_style('ol-css');
wp_enqueue_style('map-css', get_stylesheet_directory_uri() . '/css/map.css');

$post_id              = get_the_ID();
$is_page_builder_used = et_pb_is_pagebuilder_used( $post_id );
$container_tag        = 'product' === get_post_type( $post_id ) ? 'div' : 'article'; ?>

    <div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<!--<div id="left-area">-->

<?php endif; ?>

				<div id="content" class="site-content" role="main">
				<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

				<!--<header class="page-header">
					<h1 class="page-title"><?php the_title(); ?></h1>
				</header>--><!-- .entry-header -->

				<div class="entry-content">

					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; // end of the loop. ?>

					<div class="et_pb_section et_section_regular">

						<div class="et_pb_row">

							<div id="map" class="map">
								<div id="layerswitcher" class="ol-panel open">
									<div class="content">
										<div class="loader"></div>
									</div>
								</div>
							</div>
							<div id="popup" class="ol-popup">
						    	<a href="#" id="popup-closer" class="ol-popup-closer"></a>
								<div>
									<h3 id="popup-title"></h3>
									<div id="popup-content"></div>
								</div>
						    </div>

						</div>

					</div>

				</div>

			</div><!-- #content -->

<?php if ( ! $is_page_builder_used ) : ?>

			<!-- </div> #left-area -->

			<?php //get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php
	wp_register_script('ol-js', "https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.15.1/build/ol.js", array('jquery'), '6.15.1', true);
	wp_enqueue_script('ol-js');
	wp_enqueue_script('map-js', get_stylesheet_directory_uri() . '/js/map.js', array('ol-js'), '1.0', true);

get_footer();
