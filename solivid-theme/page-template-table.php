<?php
/*
Template Name: Data Table
*/

get_header();

wp_register_style('datatable-css', "https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css", array(), '1.10.16');
wp_enqueue_style('datatable-css');

$post_id              = get_the_ID();
$is_page_builder_used = et_pb_is_pagebuilder_used( $post_id );
$container_tag        = 'product' === get_post_type( $post_id ) ? 'div' : 'article'; ?>

    <div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<!--<div id="left-area">-->

<?php endif; ?>

				<div id="content" class="site-content" role="main">
				<a class="close-button" href="<?php echo esc_url( home_url( '/' ) ); ?>">×</a>

				<header class="page-header">
					<h1 class="page-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->

				<div class="entry-content">

					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; // end of the loop. ?>

					<div id="datatable"></div>

				</div>

			</div><!-- #content -->

<?php if ( ! $is_page_builder_used ) : ?>

			<!-- </div> #left-area -->

			<?php //get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

wp_register_script('datatable-js', "https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js", array( 'jquery' ), '1.10.16', true);
wp_enqueue_script('datatable-js');
wp_enqueue_script( 'table-js', get_stylesheet_directory_uri() . '/js/table.js', array('datatable-js'), '1.0', true );

get_footer();
